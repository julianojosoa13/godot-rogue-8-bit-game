extends KinematicBody2D

var direction := Vector2.ZERO
var velocity := Vector2.ZERO
var speed := 100.0

var action1 := false
var can_dash := true

onready var timer := $dash_recovery
onready var dust := $dust

signal dash
signal end_dash

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	input_loop()
	
	movement_loop()
	
	animation_loop()
	
	velocity = move_and_slide(velocity, Vector2.UP)

func input_loop()->void:
	direction = Vector2(
		Input.get_action_strength("right") - Input.get_action_strength("left"),
		Input.get_action_strength("down") - Input.get_action_strength("up")
	).normalized()

	action1 = Input.is_action_just_pressed("action1")
	
func movement_loop()->void:
	if action1 and can_dash and direction != Vector2.ZERO:
		velocity = direction * (speed * 12.5)
		timer.start()
		can_dash = false
		dust.global_position = (direction.rotated(deg2rad(180)) * 25) + global_position
		dust.emitting = true
		emit_signal("dash")
	else :
		velocity = lerp(velocity, direction * speed, 0.15)
		
func animation_loop()->void:
	if direction == Vector2.ZERO:
		$Sprite.play("idle")
	elif direction.x != 0:
		$Sprite.play("right" if direction.x > 0 else "left" )
	elif direction.y != 0:
		$Sprite.play("down" if direction.y > 0 else "up" )
	
func _on_dash_recovery_timeout():
	can_dash = true
	emit_signal("end_dash")
