extends Node2D

var dashing := false
onready var total = $Player/dash_recovery.get_wait_time()

func _ready():
	$dash_recovery/ColorRect/ProgressBar.max_value = total
	$dash_recovery/ColorRect/ProgressBar.value = total


func _on_Player_dash():
	dashing = true
	
func _process(_delta: float) -> void:
	if dashing:	
		$dash_recovery/ColorRect/ProgressBar.max_value = total
		$dash_recovery/ColorRect/ProgressBar.value = total - $Player/dash_recovery.time_left
	


func _on_Player_end_dash():
	$dash_recovery/ColorRect/ProgressBar.max_value = total
	$dash_recovery/ColorRect/ProgressBar.value = total
	dashing = false
